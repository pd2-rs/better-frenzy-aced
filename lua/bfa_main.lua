BFAMod = BFAMod or {}

function BFAMod.rebalance_posthook()
  Hooks:PostHook(UpgradesTweakData, "_init_pd2_values", "bfa_rebalance_id",
    function (self)
      self.values.player.healing_reduction[2] = 0
      self.skill_descs.frenzy.multipro2       = "100%"
    end
  )
end

function BFAMod.uppersfix_prehook()
  Hooks:PreHook(PlayerDamage, "_check_bleed_out", "bfa_uppersfix_id",
    function (self)
	    if self:get_real_health() == 0 and not self._check_berserker_done then
        local time = Application:time()

        if not self._block_medkit_auto_revive and time > self._uppers_elapsed + self._UPPERS_COOLDOWN then
          local auto_recovery_kit = FirstAidKitBase.GetFirstAidKit(self._unit:position())
          local is_bfa = self._healing_reduction == 0

			    if auto_recovery_kit and is_bfa then
            self._uppers_elapsed = time
          end
        end
      end
    end
  )
end

if RequiredScript == "lib/tweak_data/upgradestweakdata" then
  BFAMod.rebalance_posthook()
elseif RequiredScript == "lib/units/beings/player/playerdamage" then
  BFAMod.uppersfix_prehook()
end